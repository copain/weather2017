# GitLab weather

Transformation process from the PostgreSQL table wdonnees to the weather2018
dataset (see documentation
[here](http://ontology.irstea.fr/pmwiki.php/Site/WeatherSOSA))

This dataset contains weather measurements from August 2018 up to now. It's
updated daily.

## Files
### weather2018-definitions.sparql
Dataset's metadata, using VoID.

### weather2018-resource.sparql
Contains instances for :

- weather station (sosa:Platform),
- its sensors (sosa:Sensor),
- features of interest (sosa:FeatureOfInterest)
- … *and other instances needed for this dataset*.

### society.ttl
Contains the definition of individuals that are generic:
Orgnization, Person, Location

### py/
A directory for pyhton sources.

### dump/
A directory containing dumps of the whole dataset.