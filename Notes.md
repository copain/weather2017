# Notes de travail *weather2017*
## Adresse de la station
http://meteo.clermont.irstea.priv/MeteoMontoldre/

## Interrogation de la BD
- Se logger en postgres sur cfs-capteurs
- Utiliser la BD weatherlink_db

On peut le faire en distant depuis cfs-ontology p.ex (il faut une machine
en 195.221.117.x) avec

        psql -h cfs-capteurs.clermont.irstea.priv -U irstea weatherlink_db

%(mdp : le même que pour le sudo)

Exemple de requête :

        select * from wdonnees where jourheure > '2019-06-18';

## Installation de *society*
### Ajout du dataset *society* dans fuseki
##### `/home/administrateur/fuseki-server/CONFIG/config-tdb.ttl` :

    # ===== http://ontology.irstea.fr/society/
    <#serviceSociety> rdf:type fuseki:Service;
        fuseki:name "society" ;
        fuseki:serviceQuery               "query" ;
        fuseki:serviceUpdate              "update" ;
        fuseki:serviceUpload              "upload" ;
        fuseki:serviceReadWriteGraphStore "data" ;
        fuseki:serviceReadGraphStore      "get" ;
        fuseki:dataset [
          rdf:type tdb2:DatasetTDB2;
          tdb2:location "/data2/datasets/society";
        ];
    .

Ne pas oublier l'ajout de `<#serviceSociety>` dans `fuseki:services` puis,
après sauvegarde et sortie de l'éditeur :

    sudo systemctl restart fuseki.service

### Ajout d'une instance de pubby

    cd /var/lib/tomcat7/webapps
    sudo cp -Rfv weather2017 society
    cd society/WEB-INF/

Ne pas oublier d'éditer les fichiers suivants en **root**.

##### `prefixes.rdf`

    <?xml version="1.0" encoding="utf-8"?>
    <rdf:RDF
         xmlns:conf="http://richard.cyganiak.de/2007/pubby/config.rdf#"
         xmlns:irstea="http://ontology.irstea.fr/society/"
         xmlns:irstea_org="http://ontology.irstea.fr/society/organization/"
         xmlns:irstea_address="http://ontology.irstea.fr/society/address/"
         xmlns:irstea_commune="http://ontology.irstea.fr/society/commune/"
         xmlns:irstea_person="http://ontology.irstea.fr/society/person/"
         xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
         xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
         xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
         xmlns:owl="http://www.w3.org/2002/07/owl#"
         xmlns:dc="http://purl.org/dc/elements/1.1/"
         xmlns:dcterms="http://purl.org/dc/terms/"
         xmlns:foaf="http://xmlns.com/foaf/0.1/"
         xmlns:skos="http://www.w3.org/2004/02/skos/core#"
         xmlns:geo="http://www.opengis.net/ont/geosparql#"
         xmlns:locn="http://www.w3.org/ns/locn#"
         xmlns:qudt="http://qudt.org/1.1/schema/qudt#"
         xmlns:void="http://rdfs.org/ns/void#"
    ></rdf:RDF>

##### `config.ttl`

- Déclaration des préfixes (les mêmes que dans prefixes.rdf, en changeant juste
  le format).
- Ne pas oublier le
      @prefix conf: <http://richard.cyganiak.de/2007/pubby/config.rdf#> .
- On change :
  - conf.webBase
  - conf:usePrefixesFrom
  - conf:indexResource
  - conf:sparqlEndpoint
  - conf:datasetBase

Pour finir :

    sudo systemctl restart tomcat7.service

### Ajout des règles dans le proxy apache
##### `/etc/apache2/sites-enabled/proxy-jena.conf`

       # ==== Society
       # -- Sparql endpoint
       ProxyPass /society/query http://127.0.0.1:3030/society/query
       ProxyPassReverse /society/query http://127.0.0.1:3030/society/query
       ProxyPass /society/sparql http://127.0.0.1:3030/society/sparql
       ProxyPassReverse /society/sparql http://127.0.0.1:3030/society/sparql

       # -- Pubby
       ProxyPass /society http://127.0.0.1:8080/society
       ProxyPassReverse /society http://127.0.0.1:8080/society

Puis :

    sudo systemctl reload apache2.service


### Insertion des données dans le triplestore

    ${JENA_HOME}/bin/s-put \
      'http://cfs-sparql.clermont.cemagref.fr:3030/society/data' \
      default society.ttl
    ${JENA_HOME}/bin/s-update \
      --service='http://cfs-sparql.clermont.cemagref.fr:3030/society/update' \
      --update society-2018-12.sparql


# Notes sur *pg2rdf*
## Mise en œuvre
### La première fois
    # ZZZ Après avoir vidé le dataset ZZZ

    $JENA_HOME/bin/s-update \
    --service 'http://cfs-sparql.clermont.cemagref.fr:3030/weather/update' \
    --update ../weather-definitions.sparql

    $JENA_HOME/bin/s-update \
    --service 'http://cfs-sparql.clermont.cemagref.fr:3030/weather/update' \
    --update ../weather-resource.sparql

    #### À exécuter sur cfs-sparql
    cd /home/administrateur/meteo/py
    python3 pg2rdf.py

### Ajout des outils sur cfs-sparql (pubby, snorql)
#### Snorql
Il est dans /var/www/html/weather

RQ : À cause de l'URL (externe) du sparql endpoint dans la config, il faut
que le proxy soit à jour pour tester la config de pubby.

#### Pubby
Et ça c'est dans /var/lib/tomcat7/webapps/weather

### Pour qu'il se mette à jour
    crontab -e

    ## Ajout de la ligne :
    54 3 * * * cd /home/administrateur/meteo/py && python3 pg2rdf.py

##### Vérif:
    SELECT (max(?time) AS ?t) WHERE { ?o <http://www.w3.org/ns/sosa/resultTime> ?time }

Aujourd'hui :
"2019-07-07T23:50:00+01:00"
"2019-07-07T23:50:00+01:00"
On verra demain…
