# py/ directory
## pg2rdf.py
#### *en*
The OPEROSE project needs a daily updated database containing collected
weather data. This is done in a postgresql relational database.
**pg2rdf** is to be ran once a day in a *cron* task to add new data
in the triplestore. It does a request to the relational database to retreive
measurments from the latest observation stored in the triplestore and updates
the rdf dataset with them.

The **pg2rdf** code is based on insertWeatherData's code, which is located
in the branch named *weather2017*.

The files for pg2rdf are prefixed with **p2r_**.

#### *fr*
Dans le cadre du projet OPEROSE, les données de la station météo sont stockées
jour par jour dans une BD relationnelle de type postgresql.
**pg2rdf** vise à être lancé une fois par jour (dans une tâche cron)
pour ajouter au triplestore les données de la base Postgresql dont la date est
ultérieure à la donnée la plus récente du triplestore.

Le code  de pg2rdf reprend celui de insertWeatherData.

Les fichiers relatifs à pg2rdf sont préfixés **p2r_**.

## Usage
### First use

- Edit p2tr_config.py to adjust parameters,
- Create a p2r_password.py file containing only

        PG_PASSWORD = "xxxxxx"

    which is the password for the relational database.

- Ensure the triplestore is empty,
- then run :

        $JENA_HOME/bin/s-update \
        --service 'http://cfs-sparql.clermont.cemagref.fr:3030/weather/update' \
        --update ../weather2018-definitions.sparql

        $JENA_HOME/bin/s-update \
        --service 'http://cfs-sparql.clermont.cemagref.fr:3030/weather/update' \
        --update ../weather2018-resource.sparql


### Triplestore updates

        #### Is run on the triplestore server itself.
        cd /home/administrateur/meteo/py
        python3 pg2rdf.py
        python3 updateTemporal.py
