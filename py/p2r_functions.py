import sys
import re
from datetime import datetime
import arrow    # For datetime timezones
import requests # HTTP protocol

from p2r_config import BASE_PREFIX, TIMEZONE
from p2r_config import WIND_DIRECTIONS #, NO_VALUE
from p2r_config import INDEX_OF, CONTEXT, UNIT
from p2r_config import SPARQL_UPDATE_ENDPOINT

# Prints to stderr. Copypasted from
# https://stackoverflow.com/questions/5574702/how-to-print-to-stderr-in-python
def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


# Does an HTTP POST operation in order to do a sparql update request.
def do_sparql_update(sparql):
    query = { 'update': sparql }
    r = requests.post(SPARQL_UPDATE_ENDPOINT, data=query)
    if (r.status_code != 200) :
        eprint('>>>>>>> ERROR <<<<<<<')
        eprint(r.text)
        eprint('--- query:')
        eprint(sparql)
        eprint('----------')


# Converts "prefix dummy: <http://example/>"
# as "@prefix dummy: <http://example/> ."
# Works with multiline prefixes such as OBSERVATION_PREFIX.
# Simply replaces 'prefix ' with '@prefix ' and '>' with '> .'.
def sparqlPrefix_to_ttl(prefixes):
    return re.sub('>', '> .',re.sub(r'prefix ', '@prefix ', prefixes))


RESULT_STACK = []
def feature_to_ttl(feature, value, time, interval):
    if value is None:
        return ''

    CTX = CONTEXT[feature] # To make code shorter
    if (CTX['phenomenonTime'] == 'interval'): dt = interval
    else: dt = time

    # The Result
    rr = "%s%s" % (value, UNIT[feature]['unit_txt'])
    try:
        dummy = RESULT_STACK.index(rr)
        result="""<%sresult/value_%s_%s> sosa:isResultOf <%sobservation/at_%s_of_%s_on_%s> .

""" % (BASE_PREFIX, value, UNIT[feature]['unit_txt'],
               BASE_PREFIX, dt, CTX['madeBySensor'], CTX['observedProperty'])
    except:
        RESULT_STACK.append(rr)
        result = """<%sresult/value_%s_%s> a sosa:Result,
    qudt:QuantityValue;
    qudt:unit <%s> ;
    qudt:numericValue "%s"^^xsd:double ;
    sosa:isResultOf <%sobservation/at_%s_of_%s_on_%s> .

""" % (BASE_PREFIX, value, UNIT[feature]['unit_txt'],
            UNIT[feature]['unit'], value,
            BASE_PREFIX, dt, CTX['madeBySensor'], CTX['observedProperty'])

    # The Observation
    obs = """<%sobservation/at_%s_of_%s_on_%s> a sosa:Observation ;
    sosa:observedProperty <%sobservableProperty/%s> ;
    sosa:hasFeatureOfInterest <%sfeatureOfInterest/%s> ;
    sosa:madeBySensor <%ssensor/%s> ;
    sosa:hasResult <%sresult/value_%s_%s> ;
    sosa:phenomenonTime <%s%s/%s> ;
    sosa:resultTime "%s"^^xsd:dateTime ;
    sosa:hasSimpleResult "%s %s"^^cdt:%s .

""" % (BASE_PREFIX, dt, CTX['madeBySensor'], CTX['observedProperty'],
        BASE_PREFIX, CTX['observedProperty'], # sosa:observedProperty
        BASE_PREFIX, CTX['featureOfInterest'], # sosa:hasFeatureOfInterest
        BASE_PREFIX, CTX['madeBySensor'], # sosa:madeBySensor
        BASE_PREFIX, value, UNIT[feature]['unit_txt'], # sosa:hasResult
        BASE_PREFIX, CTX['phenomenonTime'], dt, # sosa:phenomenonTime
        time, # sosa:resultTime
        value, UNIT[feature]['unit_symbol'], UNIT[feature]['cdt_type'],
      )

    mobs = """<%ssensor/%s> sosa:madeObservation
    <%sobservation/at_%s_of_%s_on_%s> .

""" % (BASE_PREFIX, CTX['madeBySensor'],
        BASE_PREFIX, dt, CTX['madeBySensor'], feature.replace(' ','_'))

    return result + obs #+ mobs


# Three lists for stacking latest time objects created, to avoid
# multiple declaration of same time objects.
MAX_TIME_STACK_LENGTH = 10000
INSTANT_STACK = []
INTERVAL_STACK = []
DURATION_STACK = []

def get_time_instant(dt):
    dt_xsd = str(arrow.get(dt, TIMEZONE))
    return """<%sinstant/%s> a time:Instant ;
    time:inXSDDateTimeStamp "%s"^^xsd:dateTimeStamp ;
    time:inDateTime <%sdateTimeDescription/%s> .

""" % (BASE_PREFIX, dt_xsd, dt_xsd, BASE_PREFIX, dt_xsd)

def get_date_time_description(dt):
    dt_xsd = str(arrow.get(dt, TIMEZONE))
    return """<%sdateTimeDescription/%s> a time:DateTimeDescription ;
    time:unitType time:unitMinute ;
    time:year "%s"^^xsd:gYear ; time:month "--%02d"^^xsd:gMonth ;
    time:day "---%02d"^^xsd:gDay ;
    time:hour "%s"^^xsd:nonNegativeInteger ;
    time:minutes "%s"^^xsd:nonNegativeInteger .

""" % (BASE_PREFIX, dt_xsd, dt.year, dt.month,
        dt.day, dt.hour, dt.minute)

def get_duration(dur):
    return """<%sduration/PT%sM> a time:Duration ;
    time:unitType time:unitMinute ;
    time:numericDuration "%s"^^xsd:decimal .

""" % (BASE_PREFIX, dur, dur)



# Returns the ttl for date and time insertions. May be empty for
# elements (time:Instants/Intervals/Durations) already returned
# by a previous call to this function.
def insert_time_objects(begin_time, end_time):
    duration_minutes = int((end_time - begin_time).total_seconds() / 60)

    # Begin time should be already in the database in most cases.
    # Testing it before inserting would be more elegant but triples already
    # in won't be inserted twice (tested on jena/fuseki)
    begin_xsd = str(arrow.get(begin_time, TIMEZONE))
    try:
        dummy = INSTANT_STACK.index(begin_xsd)
        begin_str = ''
        begin_desc = ''
    except:
        INSTANT_STACK.append(begin_xsd)
        begin_str = get_time_instant(begin_time)
        begin_desc = get_date_time_description(begin_time)

    # La sama faro, sed "begin" estas "end" ĉi tie.
    end_xsd = str(arrow.get(end_time, TIMEZONE))
    try:
        dummy = INSTANT_STACK.index(end_xsd)
        end_str = ''
        end_desc = ''
    except:
        INSTANT_STACK.append(end_xsd)
        end_str = get_time_instant(end_time)
        end_desc = get_date_time_description(end_time)
        while (len(INSTANT_STACK) > MAX_TIME_STACK_LENGTH):
            INSTANT_STACK.pop(0)

    dur_str = ''
    try:
        dummy = DURATION_STACK.index(duration_minutes)
    except:
        DURATION_STACK.append(duration_minutes)
        dur_str = get_duration(duration_minutes)
        # Rq : On ne vide pas la DURATION_STACK, qui ne bénéficie pas de l'ordre
        #      cronologique du fichier csv. Doit pas être très lourd de ttes façons.

    inter_str = "PT%dM_%s" % (duration_minutes, begin_xsd)
    interval = ''
    try:
        dummy = INTERVAL_STACK.index(inter_str)
    except:
        INTERVAL_STACK.append(inter_str)
        interval = """<%sinterval/%s> a time:Interval ;
    time:hasDuration <%sduration/PT%sM> ;
    time:hasBeginning <%sinstant/%s> ;
    time:hasEnd <%sinstant/%s> .

""" % (BASE_PREFIX, inter_str, BASE_PREFIX, duration_minutes,
        BASE_PREFIX, begin_xsd, BASE_PREFIX, end_xsd)

        while (len(INTERVAL_STACK) > MAX_TIME_STACK_LENGTH):
            INTERVAL_STACK.pop(0)

    return """%s%s%s%s%s%s
""" % (begin_str, begin_desc, end_str, end_desc, dur_str, interval)



def get_ttl(line, duration):
    # Get ttl for time objects
    end_time = line[0]
    begin_time = end_time - duration
    ttl_time = insert_time_objects(begin_time, end_time)

    # Get time objects names (instant and interval)
    end_xsd = str(arrow.get(end_time, TIMEZONE))
    inter_str = "PT%dM_%s" % (int(duration.total_seconds() / 60),
                              str(arrow.get(begin_time, TIMEZONE)))

    # Get ttl for each feature
    temp = feature_to_ttl('temperature',    line[1], end_xsd, inter_str)
    pres = feature_to_ttl('airPressure',    line[2], end_xsd, inter_str)
    humi = feature_to_ttl('humidity',       line[3], end_xsd, inter_str)
    rain = feature_to_ttl('rainfallAmount', line[4], end_xsd, inter_str)
    rate = feature_to_ttl('rainfallRate',   line[5], end_xsd, inter_str)
    wind = feature_to_ttl('windSpeed',      line[6], end_xsd, inter_str)
    solr = feature_to_ttl('solarRadiation', line[8], end_xsd, inter_str)

    if line[7] is not None:
        wdir = feature_to_ttl('windDirection', WIND_DIRECTIONS[line[7].strip()],
                              end_xsd, inter_str)
    else:
        wdir = ''


    return """%s
%s
%s
%s
%s
%s
%s
%s
%s
""" % (ttl_time, temp, pres, humi, rain, rate, wind, wdir, solr)

def empty_stacks():
    RESULT_STACK = []
    INSTANT_STACK = []
    INTERVAL_STACK = []
    DURATION_STACK = []
