
WRITE_TO_TRIPLESTORE = True # If False, writes ttl to standard output
QUIET = False # If True, should be silent (only errors are reported).

SPARQL_QUERY_ENDPOINT = "http://cfs-sparql.clermont.cemagref.fr:3030/weather/query"
SPARQL_UPDATE_ENDPOINT = "http://cfs-sparql.clermont.cemagref.fr:3030/weather/update"

PG_HOST = "cfs-capteurs.clermont.irstea.priv"
PG_DATABASE = "weatherlink_db"
PG_USER = "irstea"
#PG_PASSWORD = "xxxxxxx"


BASE_PREFIX = 'http://ontology.irstea.fr/weather/resource/'

# The timezone is the timezone used by the weather station.
TIMEZONE = '+0100' # +0100 = heure d'hiver. 'Europe/Paris' = ajustement auto été/hiver

OBSERVATION_PREFIX="""prefix sosa: <http://www.w3.org/ns/sosa/>
prefix xsd: <http://www.w3.org/2001/XMLSchema#>
prefix qudt: <http://qudt.org/1.1/schema/qudt#>
prefix qudt_unit: <http://qudt.org/1.1/vocab/unit#>
prefix cdt: <http://w3id.org/lindt/custom_datatypes#>
"""
TIME_PREFIX = """prefix time: <http://www.w3.org/2006/time#>
prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix xsd: <http://www.w3.org/2001/XMLSchema#>
"""

INTERVAL_MAX = 100 # If duration beween lines > INTERVAL_MAX minutes,
                   # don't consider it's a valid interval.


INDEX_OF = {
  'date': 0,
  'time': 1,
  'temperature': 2,
  'humidity': 5,
  'windSpeed': 7,
  'windDirection': 8,
  'airPressure': 16,
  'rainfallAmount': 17,
  'rainfallRate': 18,
  'solarRadiation': 19
}

CONTEXT = {
'temperature':
  {  'observedProperty': 'air_temperature',
     'madeBySensor': 'VP2lesPalaquins01_outsideTemperatureSensor01',
     'featureOfInterest': 'air',
     'phenomenonTime': 'instant',
  },
'humidity':
  {  'observedProperty': 'air_relativeHumidity',
     'madeBySensor': 'VP2lesPalaquins01_outsideHumiditySensor01',
     'featureOfInterest': 'air',
     'phenomenonTime': 'instant',
  },
'windSpeed':
  {  'observedProperty': 'wind_speed',
     'madeBySensor': 'VP2lesPalaquins01_anemometer01',
     'featureOfInterest': 'wind',
     'phenomenonTime': 'interval',
  },
'windDirection':
  {  'observedProperty': 'wind_direction',
     'madeBySensor': 'VP2lesPalaquins01_windDirectionSensor01',
     'featureOfInterest': 'wind',
     'phenomenonTime': 'interval',
  },
'airPressure':
  {  'observedProperty': 'air_pressure',
     'madeBySensor': 'VP2lesPalaquins01_barometer01',
     'featureOfInterest': 'air',
     'phenomenonTime': 'instant',
  },
'rainfallAmount':
  {  'observedProperty': 'precipitation_amount',
     'madeBySensor': 'VP2lesPalaquins01_rainCollectorSensor01',
     'featureOfInterest': 'precipitation',
     'phenomenonTime': 'interval',
  },
'rainfallRate':
  {  'observedProperty': 'precipitation_rate',
     'madeBySensor': 'VP2lesPalaquins01_rainCollectorSensor01',
     'featureOfInterest': 'precipitation',
     'phenomenonTime': 'interval',
  },
'solarRadiation':
  {  'observedProperty': 'solarFlux_density',
     'madeBySensor': 'VP2lesPalaquins01_solarRadiationSensor01',
     'featureOfInterest': 'solarFlux',
     'phenomenonTime': 'interval',
  },
}

UNIT = {
'temperature':
  {  'unit': 'http://qudt.org/1.1/vocab/unit#DegreeCelsius',
     'unit_txt': 'degreeCelcius', # For sosa:Results naming
     'unit_symbol': 'CEL', 'cdt_type': 'temperature',
  },
'humidity':
  {  'unit': 'http://qudt.org/1.1/vocab/unit#Percent',
     'unit_txt': 'percent',
     'unit_symbol' : '%', 'cdt_type': 'ucum',
  },
'windSpeed':
  {  'unit': 'http://qudt.org/1.1/vocab/unit#MeterPerSecond',
     'unit_txt': 'meterPerSecond',
     'unit_symbol' : 'm/s', 'cdt_type': 'ucum',
  },
'windDirection':
  {  'unit': 'http://qudt.org/1.1/vocab/unit#DegreeAngle',
     'unit_txt': 'degreeAngle',
     'unit_symbol' : 'DEG', 'cdt_type': 'ucum',
  },
'airPressure':
  {  'unit': 'http://qudt.org/1.1/vocab/unit#Millibar',
     'unit_txt': 'millibar',
     'unit_symbol' : 'HPAL', 'cdt_type': 'ucum',
  },
'rainfallAmount':
  {  'unit': 'http://qudt.org/1.1/vocab/unit#Millimeter',
     'unit_txt': 'millimeter',
     'unit_symbol' : 'mm', 'cdt_type': 'length',
  },
'rainfallRate':
  {  'unit': 'http://ontology.irstea.fr/weather2017/resource/unit/MillimeterPerHour',
     'unit_txt': 'millimeterPerHour',
     'unit_symbol' : 'mm/h', 'cdt_type': 'ucum',
  },
'solarRadiation':
  {  'unit': 'http://qudt.org/1.1/vocab/unit#WattPerSquareMeter',
     'unit_txt': 'wattPerSquareMeter',
     'unit_symbol' : 'W/m2', 'cdt_type': 'ucum',
  },
}

WIND_DIRECTIONS = {
  'N': 000.0, 'NNE': 022.5, 'NE': 045.0, 'ENE': 067.5,
  'E': 090.0, 'ESE': 112.5, 'SE': 135.0, 'SSE': 157.5,
  'S': 180.0, 'SSW': 202.5, 'SW': 225.0, 'WSW': 247.5,
  'W': 270.0, 'WNW': 292.5, 'NW': 315.0, 'NNW': 337.5,
}
