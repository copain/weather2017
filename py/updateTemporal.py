import json
import requests
from datetime import datetime
from p2r_config import SPARQL_UPDATE_ENDPOINT
from p2r_config import SPARQL_QUERY_ENDPOINT

VERBOSE = True
DO_UPDATES = True

###
### --------------- time:hasEnd ------------------
###

if VERBOSE:
    print(">>> time:hasEnd")

query = {'update': """
WITH <http://ontology.irstea.fr/weather/metadata>
DELETE { <http://ontology.irstea.fr/weather/metadata/from_2018-08-01T00:10:00+01:00>
         <http://www.w3.org/2006/time#hasEnd> ?c }
WHERE { GRAPH <http://ontology.irstea.fr/weather/metadata> {
         <http://ontology.irstea.fr/weather/metadata/from_2018-08-01T00:10:00+01:00>
         <http://www.w3.org/2006/time#hasEnd> ?c } }
"""}

if DO_UPDATES:
  r = requests.post(SPARQL_UPDATE_ENDPOINT, data=query)
  if (r.status_code != 200) :
      print(r.text)
      exit(-1)

query = {'query': """SELECT (MAX(?ts) AS ?last) WHERE {
[] a <http://www.w3.org/2006/time#Instant> ;
<http://www.w3.org/2006/time#inXSDDateTimeStamp> ?ts }"""}
r = requests.get(SPARQL_QUERY_ENDPOINT,  params=query)
j = json.loads(r.text)

if VERBOSE:
  print("    Last instant : %s" % j['results']['bindings'][0]['last']['value'])

query = {'update': """INSERT DATA {
GRAPH <http://ontology.irstea.fr/weather/metadata> {
  <http://ontology.irstea.fr/weather/metadata/from_2018-08-01T00:10:00+01:00>
    <http://www.w3.org/2006/time#hasEnd>
    <http://ontology.irstea.fr/weather/resource/instant/%s> }
}""" % j['results']['bindings'][0]['last']['value']}

if DO_UPDATES:
  r = requests.post(SPARQL_UPDATE_ENDPOINT, data=query)
  if (r.status_code != 200) :
      print(r.text)
      exit(-1)

if VERBOSE:
  print("    Done.")
  print("")


###
### --------------- void:triple ------------------
###
if VERBOSE:
    print(">>> void:triple")

query = {'query': "SELECT (COUNT(*) AS ?nb) WHERE { ?a ?b ?c }"}
r = requests.get(SPARQL_QUERY_ENDPOINT,  params=query)
j = json.loads(r.text)

if VERBOSE:
  print("    %s triples." % j['results']['bindings'][0]['nb']['value'])

query = {'update': """WITH <http://ontology.irstea.fr/weather/metadata>
DELETE { ?a <http://rdfs.org/ns/void#triples> ?n }
INSERT { ?a <http://rdfs.org/ns/void#triples> %s }
WHERE { ?a a <http://rdfs.org/ns/void#Dataset> ;
           <http://rdfs.org/ns/void#triples> ?n
}""" % j['results']['bindings'][0]['nb']['value']}

if DO_UPDATES:
  r = requests.post(SPARQL_UPDATE_ENDPOINT, data=query)
  if (r.status_code != 200) :
      print(r.text)
      exit(-1)

if VERBOSE:
  print("    Done.")
  print("")

###
### --------------- void:distinctSubjects ------------------
###
if VERBOSE:
    print(">>> void:distinctSubjects")

query = {'query': "SELECT (COUNT(DISTINCT ?s) AS ?nb) WHERE { ?s a ?c }"}
r = requests.get(SPARQL_QUERY_ENDPOINT,  params=query)
j = json.loads(r.text)

if VERBOSE:
  print("    %s subjects." % j['results']['bindings'][0]['nb']['value'])

query = {'update': """WITH <http://ontology.irstea.fr/weather/metadata>
DELETE { ?a <http://rdfs.org/ns/void#distinctSubjects> ?n }
INSERT { ?a <http://rdfs.org/ns/void#distinctSubjects> %s }
WHERE { ?a a <http://rdfs.org/ns/void#Dataset> ;
           <http://rdfs.org/ns/void#distinctSubjects> ?n
}""" % j['results']['bindings'][0]['nb']['value']}

if DO_UPDATES:
  r = requests.post(SPARQL_UPDATE_ENDPOINT, data=query)
  if (r.status_code != 200) :
      print(r.text)
      exit(-1)

if VERBOSE:
  print("    Done.")
  print("")


###
### --------------- dcterms:modified ------------------
###
dt = datetime.now()
dts = "\"%4d-%02d-%02d\"^^xsd:date" % (dt.year, dt.month, dt.day)
if VERBOSE:
    print(">>> dcterms:modified")
    print("    %s" % dts)

query = {'update': """PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
WITH <http://ontology.irstea.fr/weather/metadata>
DELETE { ?a <http://purl.org/dc/terms/modified> ?d }
INSERT { ?a <http://purl.org/dc/terms/modified> %s }
WHERE { ?a a <http://rdfs.org/ns/void#Dataset> ;
           <http://purl.org/dc/terms/modified> ?d . }""" % dts}

if DO_UPDATES:
  r = requests.post(SPARQL_UPDATE_ENDPOINT, data=query)
  if (r.status_code != 200) :
      print(r.text)
      exit(-1)

if VERBOSE:
  print("    Done.")
  print("")
