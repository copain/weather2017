#!/usr/bin/python3

# Destiné à être exécuté périodiquement sur cfs-sparql.
# Extrait de la BD Operose les données de la station météo
# et les insère dans le sparql endpoint.

import sys
import psycopg2
import requests
import json

from datetime import datetime

from p2r_config import *
from p2r_functions import *
from p2r_password.py import PG_PASSWORD


conn = psycopg2.connect(host=PG_HOST, database=PG_DATABASE,
                        user=PG_USER, password=PG_PASSWORD)
cur = conn.cursor()

query = {'query': 'SELECT (max(?time) AS ?t) WHERE { ?o <http://www.w3.org/ns/sosa/resultTime> ?time }'}
r = requests.get(SPARQL_QUERY_ENDPOINT,  params=query)
j = json.loads(r.text)
if (len(j['results']['bindings'][0]) == 0):
    cur.execute("SELECT jourheure, tabri, pmer, uabri, pluie, intenspluie, ventmoy, dirventmoy, raysolaire FROM wdonnees ORDER BY jourheure")
else:
    dt_start = j['results']['bindings'][0]['t']['value']
    # then we retreive only fresher data from postgres.
    cur.execute("SELECT jourheure, tabri, pmer, uabri, pluie, intenspluie, ventmoy, dirventmoy, raysolaire FROM wdonnees WHERE jourheure >= '%s' ORDER BY jourheure" % dt_start)

first_pass = True
prev_row = cur.fetchone()
if (prev_row is None):
    print("No data read.")
    cur.close()
    conn.close()
    sys.exit(0)

curr_row = cur.fetchone()
while curr_row is not None:
    if (curr_row[0].year != prev_row[0].year):
        empty_stacks()
    duration = curr_row[0] - prev_row[0]
    duration_minutes = int((curr_row[0] - prev_row[0]).total_seconds() / 60)
    if first_pass and not WRITE_TO_TRIPLESTORE:
        ttl = "%s %s" % (sparqlPrefix_to_ttl(OBSERVATION_PREFIX),
                         sparqlPrefix_to_ttl(TIME_PREFIX))
    else:
        ttl = ''
    if ((duration_minutes > 0) and (duration_minutes <= INTERVAL_MAX)):
        if WRITE_TO_TRIPLESTORE:
            print(curr_row[0])
        if (first_pass):
            ttl = get_ttl(prev_row, duration)
            ttl = "%s %s" % (ttl, get_ttl(curr_row, duration))
        else :
            ttl = get_ttl(curr_row, duration)


    if WRITE_TO_TRIPLESTORE:

        ttl = """%s
%s
INSERT DATA {
GRAPH <%sobservation/%s> {
%s
}}
""" % (OBSERVATION_PREFIX, TIME_PREFIX, BASE_PREFIX, curr_row[0].year, ttl)
        do_sparql_update(ttl)

    else:
        if not QUIET:
            print(ttl)

    first_pass = False
    prev_row = curr_row
    curr_row = cur.fetchone()

cur.close()
conn.close()
